import React, { Component } from 'react';

import Svg from './Svg';

import arrow from '../images/arrow.png';

import img1 from '../images/rightbar-sec1.jpg';
import img2 from '../images/rightbar-sec2.jpg';
import img3 from '../images/rightbar-sec3.jpg';

import boy from '../images/right-bar-boy.png';
import brand from '../images/right-bar-brand-img1.png';
import like from '../images/right-bar-like.png';
import megafone from '../images/right-bar-megafone.png';
import wheelchair from '../images/right-bar-wheelchair.png';

class RightBar extends Component {
  render(){
    return(
      <div className="rightBar">

        <div className="sections">
          <div className="arrowLeft">
            <img src={arrow} />
          </div>
          <section>
            <img className="mainImg" src={img1} />
            <div class="infos">
              <button className="support">
                <img src={like} /> Apoiar projeto
              </button>
              <div className="text">
                Fabio de Melo apoia
              </div>
              <div className="supportType">
                <img src={boy} />
                <span>Paternidade<br />no mundo</span>
              </div>
              <div className="brand">
                <img src={brand} />
              </div>
            </div>
          </section>
          <div className="arrowRight">
            <img src={arrow} />
          </div>
        </div>

        <div className="sections">
          <div className="arrowLeft">
            <img src={arrow} />
          </div>
          <section>
            <img className="mainImg" src={img2} />
            <div class="infos">
              <button className="support">
                <img src={megafone} /> Assinar Petição
              </button>
              <div className="text">
                Coração de Maria <br /> assinou
              </div>
              <div className="supportType">
                <img src={wheelchair} />
                <span>A favor da Lei da<br />Inclusão Infantil</span>
              </div>
            </div>
          </section>
          <div className="arrowRight">
            <img src={arrow} />
          </div>
        </div>

        <div className="sections">
          <div className="arrowLeft">
            <img src={arrow} />
          </div>
          <section>
            <img className="mainImg" src={img3} />
            <div class="infos">
              <button className="support white">
                <Svg width="16" height="16" type="cash" color="#b17401" /> Doar agora
              </button>
              <div className="text">
                
              </div>
              <div className="supportType">
                <img src={boy} />
                <span>Ajude nossos<br />projetos sociais</span>
              </div>
            </div>
          </section>
          <div className="arrowRight">
            <img src={arrow} />
          </div>
        </div>


      </div>
    );
  }
}

export default RightBar;
