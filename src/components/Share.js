import React, { Component } from 'react';

import attach from '../images/feed-attachment.png';
import publish from '../images/feed-btn-publish.png';
import emoji from '../images/feed-emoji.png';
import flag from '../images/feed-flag.png';
import photo from '../images/feed-photo.png';

class Share extends Component {
  render(){
    return(
      <div className="shareComponent">
        <form>
          <select>
            <option selected="selected" disabled="disabled">Assunto...</option>
            <option>Ações</option>
            <option>Fome</option>
          </select>
          <textarea placeholder="Compartilhe novidades...">
          </textarea>
          <div class="actions">
            <button type="image" className="add image"><img src={photo} alt="Inserir Imagem" /></button>
            <button type="image" className="add attach"><img src={attach} alt="Inserir Anexo" /></button>
            <button type="image" className="add emoji"><img src={emoji} alt="Inserir Emoji" /></button>
            <button type="image" className="add flag"><img src={flag} alt="Inserir Flag" /></button>
            <button type="submit" className="publish"><img src={publish} alt="Publicar" /></button>
            <div className="clear chars">
              <span>0</span> de 4000 caracteres
            </div>
          </div>
        </form>
      </div>
    );
  };
}

export default Share;
