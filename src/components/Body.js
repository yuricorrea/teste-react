import React, { Component } from 'react';

import RightBar from './RightBar';
import LeftBar from './LeftBar';
import MainContent from './MainContent';

class Body extends Component {
  render(){
    return(
      <main>
        <div className="content">
          <LeftBar />
          <MainContent />
          <RightBar />
          <div className="clear"></div>
        </div>
      </main>
    );
  }
}

export default Body;
