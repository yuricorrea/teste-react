import React, { Component } from 'react';

class LeftBar extends Component {
  render(){
    return(
      <div className="leftBar">
        <div className="section about">
          <ul>
            <li className="active">
              <h3><a href="javascript:void(0)">Sobre</a></h3>
            </li>
            <li className="hidden">
              <h3></h3>
            </li>
            <li className="hidden">
              <h3></h3>
            </li>
          </ul>
          <div className="sectionsAbout">
            <section>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur
              </p>
              <p>
                Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum
              </p>
            </section>
          </div>
        </div>
      </div>
    );
  }
}

export default LeftBar;
