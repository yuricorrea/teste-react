import React, { Component } from 'react';

import Share from './Share';
import Feed from './Feed';

import profilePhoto from '../images/feed-profile-picture.jpg';
import ticket from '../images/feed-ticket.png';

import postImg1 from '../images/feed-post-img1.jpg';
import postImg2 from '../images/feed-post-img2.jpg';

import retweetPicture from '../images/retweet-picture.png';

import Svg from './Svg';

class MainContent extends Component {

  render(){
    var feed = [];
    feed.push({
      name: 'P. Fabio de Melo',
      picture: <img src={profilePhoto} />,
      category: 'Ações',
      time: '23 min atrás',
      audience: 'global',
      edited: true,
      views: "6.719.921",
      content: 'Aos fiéis e fãs de Pernambuco, será um prazer fazer o show e levar Deus para seus corações. Católicos do CEU pagam meia entrada, bastam mostrar o cartaz exclusivo abaixo =)',
      more: true,
      image: postImg1,
      action: {
        name: 'Comprar Ingresso',
        image: <img src={ticket} />
      },
      comments: 2535
    });
    feed.push({
        name: 'P. Fabio de Melo',
        picture: <img src={profilePhoto} />,
        category: 'Fome',
        time: 'há 16hs',
        audience: 'global',
        edited: true,
        views: "60",
        content: 'Vamos ajudar na campanha do natal sem fome meus amigos católicos =)',
        more: false,
        image: postImg2,
        action: {
          name: 'Como Ajudar',
          image: <Svg type="heart" width="26" height="26" color="#b17401" />
        },
        comments: 5,
        retweet: {
          link: '#',
          name: 'Ceu',
          picture: <img src={retweetPicture} />,
          category: 'fome',
          time: 'ontem',
          audience: 'global',
          edited: true,
          views: "6.400",
          following: true,
          content: 'O Natal de muitas crianças pode ser digno graças a maior ação nacional que este ano completa 30 anos. Ajude compartilhando',
          comments: 5
        }
      });
    return(
      <div className="mainContent">
        <Share />
        <Feed content={feed} />
      </div>
    );
  }
}

export default MainContent;
