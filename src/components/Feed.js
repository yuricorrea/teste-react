import React, { Component } from 'react';

import global from '../images/feed-global.png';

import comments from '../images/feed-comment.png';
import retweet from '../images/feed-retweet.png';
import amem from '../images/feed-amem.png';
import cross from '../images/feed-cross.png';
import following from '../images/feed-btn-following.png';
import photo2 from '../images/feed-photo2.png';
import bird from '../images/feed-bird.png';

class Feed extends Component{
  render(){
    console.log(this.props.content);
    return(
      <div className="main">
        {
          this.props.content.map((post,i) => 
          <article>
            <header>
              <h4 className="title">{post.name}</h4>
              {post.picture}
            </header>
            <div className="feedContent">
              {info(post)}
              <div className="post">
                <p>
                  {post.content}
                </p>
                  {post.more == true && <div className="more">Ver mais</div>}
                <figure style={{background: "url("+post.image+")"}}>
                  <div className="photoAction">
                    <button>{post.action.image} {post.action.name}</button>
                  </div>         
                </figure>
                <div className="clear"></div>
              </div>
              {"undefined" !== typeof(post.retweet) &&
                <div className="retweet">
                  <div class="retweetHead">
                    {post.retweet.picture} <span>{post.retweet.name}</span>
                    {post.retweet.following == true && <img className="following" src={following} />}
                  </div>
                  {info(post.retweet)}
                  <p>
                    {post.retweet.content}
                  </p>
                  <span className="more">ver post</span>
                </div>
              }
            </div>
            <footer>
              <div className="actions">
                <div className="comments">
                  <img src={comments} />
                  <span>{post.comments} comentários</span>
                </div>
                <div className="retweet">
                  <a href="javascript:void(0)"><img src={retweet} /></a>
                </div>
                <div className="others">
                  <a className="amem" href="javascript:void(0)"><img src={amem} /></a>
                  <a className="cross" href="javascript:void(0)"><img src={cross} /></a>
                </div>
              </div>
              <div className="postFooter">
                <picture className="camera">
                  <img src={photo2} />
                </picture>
                <picture className="picture">
                  {post.picture}
                </picture>
                <picture className="bird">
                  <img src={bird} />
                </picture>
              </div>
            </footer>
          </article>
        )}
      </div>
    );
  }
}

const info = (post) => {
  return(
    <div className="info">
      <span className="category">{post.category}</span>&nbsp;&#9679;&nbsp;
      {post.time}&nbsp;&#9679;&nbsp;
      {audience(post.audience)}&nbsp;&#9679;&nbsp;
      {post.edited == true && <span><span className="edited">Editado</span>&nbsp;&#9679;&nbsp;</span>}
      {post.views} visualizações
    </div>
  );
}

const audience = (name) => {
  var img;
  switch(name){
    case 'global':
      img = global;
      break;
    default:
      img = global; 
      break;
  }
  return <img src={img} />
}

export default Feed;


/*

<div className="comments">
                  <img src={comments} />
                  <span>{post.comments} comentários</span>
                </div>
                <div className="retweet">
                  <a href="javascript:void(0)"><img src={retweet} /></a>
                </div>
                <div className="others">
                  <a href="javascript:void(0)"><img src={amem} /></a>
                  <a href="javascript:void(0)"><img src={cross} /></a>
                </div>


*/