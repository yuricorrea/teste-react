import React, { Component } from 'react';

import Svg from './Svg';

import calice from '../images/header-calice.png'
import profilePhoto from '../images/header-profile-photo.jpg';
import eye from '../images/header-eye.png';
import magnifier from '../images/svg/magnifier.svg';

import debates from '../images/header-menu-debates.png';
import enquetes from '../images/header-menu-enquetes.png';
import eventos from '../images/header-menu-eventos.png';
import timeline from '../images/header-menu-linha-do-tempo.png';
import midias from '../images/header-menu-midias.png';
import projetos from '../images/header-menu-projetos.png';
import sobre from '../images/header-menu-sobre.png';

import pin from '../images/header-pin.png';
import share from '../images/header-share.png';

import social from '../images/header-social.png';
import facebook from '../images/header-facebook.png';
import twitter from '../images/header-twitter.png';
import plus from '../images/header-plus.png';
import instagram from '../images/header-instagram.png';

class Header extends Component{
    constructor(props) {
        super(props);
        this.state = {
            searchString: ''
        };
        this.handleSearchString = this.handleSearchString.bind(this);
        this.search = this.search.bind(this);
    }

    handleSearchString(e){
        this.setState({
            searchString: e.target.value
        });
    }

    search(e){
        console.log("search string: " + this.state.searchString);
        e.preventDefault();
    }

    render(){
        return (
            /*eslint-disable no-script-url*/
            <header>
                <div className="upBar">
                    <div className="content">
                        <div className="profile">
                            <div className="photoName">
                                <div className="thePhoto">
                                    <img src={profilePhoto} alt="perfil" />
                                </div>
                                <div className="number">
                                    <span>99</span>
                                </div>
                                <span className="name">P. Fabio de Melo</span>
                            </div>
                            <div className="settings">
                                <a href="javascript:void(0)">
                                    <Svg width="16" height="16" type="gear" color="#fff" />
                                </a>
                            </div>
                        </div>
                        <div className="search">
                            <form onSubmit={this.search}>
                                <input type="text" value={this.state.searchString} onChange={this.handleSearchString} />
                                <input type="image" src={magnifier} alt="Search" />
                            </form>
                        </div>
                        <div className="notifications">
                            <div className="heart">
                                <a href="javascript:void(0)">
                                    <Svg width="17" height="17" type="heart" color="#fff" />
                                    <div className="number"><span>40</span></div>
                                </a>
                            </div>
                            <div className="box">
                                <a href="javascript:void(0)">
                                    <Svg width="17" height="17" type="box" color="#fff" />
                                    <div className="number"><span>9</span></div>
                                </a>
                            </div>
                            <div className="cash">
                                <a href="javascript:void(0)">
                                    <Svg width="17" height="17" type="cash" color="#fff" />
                                    <div className="number"><span>10</span></div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="mainHeader">
                    <div className="content">
                        <div className="profilePhoto">
                            <div className="photo">
                                <div className="dotUp">
                                    <span>Padre</span>&nbsp;
                                    <Svg width="10" height="10" type="dot" color="#f0b600" />
                                </div>
                                <div className="thePhoto">
                                    <img src={profilePhoto} alt="perfil" />
                                </div>
                                <div className="dotDown">
                                    <Svg color="#00ff00" width="10" height="10" type="dot" />&nbsp;
                                    <span>On line</span>
                                </div>
                            </div>
                            <div className="calice">
                                <img src={calice} />
                            </div>
                        </div>
                        <div className="profileInfo">
                            <div className="profile">
                                <h1>Pe. Fabio de Melo</h1>
                                <h2>Padre &#9679; Rio de Janeiro &#9679; RJ</h2>
                                <div className="actions">
                                    <button className="follow" type="button">
                                        <img src={eye} width="22" alt="seguir" />
                                        <span>Seguir</span>
                                    </button>
                                    <button className="invite" type="button">
                                        <Svg width="17" height="14" type="heart" color="#fff" />
                                        <span>Convidar para ação</span>
                                    </button>
                                </div>
                                <div className="followers">
                                    <ul>
                                        <li>
                                            <a href="javascript:void(0)">
                                                <span className="number">0</span>
                                                <span className="text">Seguindo</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">
                                                <span className="number">86</span>
                                                <span className="text">Seguidores</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">
                                                <span className="number">86</span>
                                                <span className="text">Ações</span>
                                            </a>
                                        </li>
                                    </ul>
                                    <div className="clear"></div>
                                </div>
                            </div>
                            <div className="profileButtons">
                                <button className="seeMap"><img src={pin} />Ver o seu mapa</button>
                                <button className="share"><img src={share} />Compartilhar</button>
                                <div className="socialButtons">
                                    <ul>
                                        <li><a href="javascript:void(0)"><img alt="rede social 1" src={social} /></a></li>
                                        <li><a href="javascript:void(0)"><img alt="facebook" src={facebook} /></a></li>
                                        <li><a href="javascript:void(0)"><img alt="twitter" src={twitter} /></a></li>
                                        <li><a href="javascript:void(0)"><img alt="plus" src={plus} /></a></li>
                                        <li><a href="javascript:void(0)"><img alt="instagram" src={instagram} /></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div className="profileMenu">
                                <ul>
                                    <li><a href="javascript:void(0)"><img src={timeline} alt="linha do tempo" />Linha do tempo</a></li>
                                    <li><a href="javascript:void(0)"><img src={sobre} alt="sobre" />Sobre</a></li> 
                                    <li><a href="javascript:void(0)"><img src={projetos} alt="projetos" />Projetos</a></li> 
                                    <li><a href="javascript:void(0)"><img src={debates} alt="debates" />Debates</a></li> 
                                    <li><a href="javascript:void(0)"><img src={eventos} alt="eventos" />Eventos</a></li> 
                                    <li><a href="javascript:void(0)"><img src={enquetes} alt="enquetes" />Enquetes</a></li> 
                                    <li><a href="javascript:void(0)"><img src={midias} alt="midias" />Mídias</a></li>
                                </ul>
                            </div>
                        </div>
                        <div className="clear"></div>
                    </div>
                </div>
            </header>
        );
    }
}

export default Header;