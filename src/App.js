import React, { Component } from 'react';

import Header from './components/Header';
import Body from './components/Body';

import './styles/index.css';

class App extends Component {
  render() {
    return (
      <div>
        <Header />
        <Body />
      </div>
    );
  }
}

export default App;
